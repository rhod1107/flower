User.create!(name:  "Rodel Abalajon",
             email: "rhod1107@yahoo.com",
             password:              "mypass",
             password_confirmation: "mypass",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Sheila Rodgers",
             email: "sheila@example.org",
             password:              "pass123",
             password_confirmation: "pass123",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "Yvonne Abalajon",
             email: "ennovy@yahoo.com",
             password:              "passme",
             password_confirmation: "passme",
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end